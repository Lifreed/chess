model = {
    "board":[],
    "history":[],
    "selectedCells":[],
    "selectedFigure":'',
    "raund":'w',
    "moves":[],
    "choice":[71, 72, 73, 74, 75, 76, 77, 78,
    81, 82, 83, 84, 85, 86, 87, 88],
    "request":[],
    "whitePositions":[],
    "blackPositions":[]
};

newGame();

function newGame(){
    if (document.readyState = "complete"){
        let request = new XMLHttpRequest();
        request.open('GET','board',true);
        request.addEventListener('readystatechange', function() {
          if (request.readyState === 4 && request.status === 200) {
            console.log(request);
            model.board = JSON.parse(request.responseText);
            draw();
          }
        });
        request.send();
    }
}

function cl(cell) {
    updateBoardModel(parseInt(cell.id));
}

function updateBoardModel(cellId) {
    let cell = getIdList(cellId);
    if (model.selectedCells.length === 0) {
        model.selectedCells = cell;
        model.selectedFigure = model.board[cell[0]][cell[1]];
        getRaundMoves(cell);
    }
    else{
        selectedFigureMoveTo(cell);
    }
}

function selectedFigureMoveTo(cell) {
    let id = getId(cell);
    if (model.choice.indexOf(id) !== -1){
        model.selectedCells = cell;
        model.selectedFigure = model.board[model.selectedCells[0]][model.selectedCells[1]];
        drawBoard();
        getRaundMoves(cell)
    }
    for (let i = 0; i < model.moves.length; i++) {
        if (model.moves[i][0] === cell[0] && model.moves[i][1] === cell[1]) {
            model.request = [model.selectedCells, cell];
            model.moves = [];
            draw();
            sendToServer()
        }
    }
}

function sendToServer() {
    if (document.readyState = 'complete') {
        let request = new XMLHttpRequest();
        request.open('POST', 'moves', true);
        let req = JSON.stringify(model.request);
        request.send(req);
        request.addEventListener('readystatechange', function() {
            if (request.status === 400 && (request.readyState === 4)) {
                alert(JSON.parse(request.responseText)["message"]);
                draw()
            }
            else if (request.readyState === 4 && request.status === 200){
                console.log(request);
                let answer = JSON.parse(request.responseText);
                model.history = answer["history"];
                model.board = answer["board"];
                model.whitePositions = answer["white_positions"];
                model.blackPositions = answer["black_positions"];
                draw();
                model.selectedCells = [];
                if (model.history[model.history.length - 1].slice(0, 3) === 'Мат'){
                    if (confirm(model.history[model.history.length - 1] + '! Начать новую партию?')){
                        resetModel();
                        newGame();
                    }
                    else alert('Поки')
                }
                else {
                    // if (model.raund == 'w') model.raund = 'b';
                    // else model.raund = 'w';
                    raund();
                }
            }
          });
    }

}


function draw() {
    drawBoard();
    drawHistory();
    possibleMoves()
}

function drawBoard() {
    let letter = ['', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', ''];
    let result = '';
    let boardDom = document.getElementById("board");
    result += '<table>';
    for (let i = 0; i < 10; i++) {
        result += "<tr>";
        for (let j = 0; j < 10; j++) {
            if (i === 0 || i === 9) result += "<th>" + letter[j] + "<\/th>";
            else if (j === 0 || j === 9) result +=  "<th>" + (8 - i  + 1)  + "<\/th>";
            else if ((i % 2 === 0 && j % 2 === 1) || (i % 2 === 1 && j % 2 === 0)) {
                result += "<td bgcolor='white' class='" + model.board[i - 1][j - 1] + "' id='" + i + j + "' onclick='cl(this)'><\/td>";
            }
            else result += "<td bgcolor='black' class='" + model.board[i - 1][j - 1] + "' id='" + i + j + "' onclick='cl(this)'><\/td>";
        }
        result += "<\/tr>";
    }
    result += "<\/table> ";
    boardDom.innerHTML = result;
}

function drawHistory() {
    let historyDom = document.getElementById("history");
    let result = '<center><h2>История ходов</h2>';
    for (let i = 0; i < model.history.length; i++){
        result += model.history[i] + '<br/>'
    }
    result += '</center>';
    historyDom.innerHTML = result
}

function possibleMoves() {
    for (let i = 0; i < model.moves.length; i++) {
        let id = getId(model.moves[i]);
        if ((model.moves[i][0] + model.moves[i][1]) % 2 === 0)  document.getElementById(id).style.backgroundColor = '#437237';
        else document.getElementById(id).style.backgroundColor = '#18FF8A'
    }
}

function raund() {
    model.choice = [];
    for (let i = 0; i < 8; i++){
        for (let j = 0; j < 8; j++){
            if (model.board[i][j].slice(-1) === model.raund) {
                model.choice[model.choice.length] = (i+1) * 10 + (j+1)
            }
        }
    }

    model.selectedCells = [];
}

function getRaundMoves(cell) {
    if(model.selectedFigure.slice(-1) !== model.raund){
        model.selectedCells = [];
    }
    else{
            getMoves(cell);
            figureMove(cell);
            draw()
    }
}

function getMoves(cell) {
    switch (model.selectedFigure) {
            case 'pawn_b':
                movePawn(cell);
                break;
            case 'pawn_w':
                movePawn(cell);
                break;
            case 'rook_b':
                moveRook(cell);
                break;
            case 'rook_w':
                moveRook(cell);
                break;
            case 'knight_b':
                moveKnight(cell);
                break;
            case 'knight_w':
                moveKnight(cell);
                break;
            case 'bishop_b':
                moveBishop(cell);
                break;
            case 'bishop_w':
                moveBishop(cell);
                break;
            case 'queen_b':
                moveQueen(cell);
                break;
            case 'queen_w':
                moveQueen(cell);
                break;
            case 'king_b':
                moveKing(cell);
                break;
            case 'king_w':
                moveKing(cell);
                break;
            case '':
                break;
    }
}

function figureMove(cell) {
    let figure = model.board[cell[0]][cell[1]];
    model.board[cell[0]][cell[1]] = '';
    let figureInCell = '';
    let moves = model.moves;
    for (let i = moves.length - 1; i >= 0; i--){
        figureInCell = model.board[moves[i][0]][moves[i][1]];
        model.board[moves[i][0]][moves[i][1]] = figure;
        if (enemy_moves(figure)){
            model.board[moves[i][0]][moves[i][1]] = figureInCell;
            moves.splice(i, 1);
        }
        else model.board[moves[i][0]][moves[i][1]] = figureInCell;
    }
    model.moves = moves;
    model.board[cell[0]][cell[1]] = figure;
}

function enemy_moves(figure) {
    color = figure.slice(-1);
    if (color === 'b'){
        for (let i = 0; i < model.whitePositions.length; i++){
            model.selectedFigure = model.board[model.whitePositions[i][0]][model.whitePositions[i][1]];
            getMoves(model.whitePositions[i]);
            for (let j = 0; j < model.moves.length; j++){
                if (model.board[model.moves[j][0]][model.moves[j][1]] === 'king_b'){
                    model.selectedFigure = figure;
                    return true
                }
            }
        }
    }
    else{
        for (let i = 0; i < model.blackPositions.length; i++){
            model.selectedFigure = model.board[model.blackPositions[i][0]][model.blackPositions[i][1]];
            getMoves(model.blackPositions[i]);
            for (let j = 0; j < model.moves.length; j++){
                if (model.board[model.moves[j][0]][model.moves[j][1]] === 'king_w'){
                    model.selectedFigure = figure;
                    return true
                }
            }
        }
    }
    model.figure = figure;
}

function getIdList(id) {
    return [Math.floor(id / 10) - 1, id % 10 - 1]
}

function getId(cell) {
    return (cell[0] + 1) * 10 + cell[1] + 1
}

function movePawn(cell) {
    model.moves = [];
    if (model.selectedFigure === 'pawn_b') {
        if (model.board[cell[0] + 1][cell[1]] === '') {
            model.moves[model.moves.length] = [cell[0] + 1, cell[1]]
        }
        if (cell[0] + 1 === 2 && model.board[cell[0] + 2][cell[1]] === ''
            && model.board[cell[0] + 1][cell[1]] === '') {
            model.moves[model.moves.length] = [cell[0] + 2, cell[1]]
        }
        if (cell[1] !== 7 && model.board[cell[0] + 1][cell[1] + 1].slice(-1) === 'w') {
            model.moves[model.moves.length] = [cell[0] + 1, cell[1] + 1]
        }
        if (cell[1] !== 0 && model.board[cell[0] + 1][cell[1] - 1].slice(-1) === 'w') {
            model.moves[model.moves.length] = [cell[0] + 1, cell[1] - 1]
        }
    }

    else{
        if (model.board[cell[0]-1][cell[1]] === '') {
            model.moves[model.moves.length] = [cell[0] - 1, cell[1]];
        }
        if (cell[0] + 1 === 7 && model.board[cell[0]-2][cell[1]] === ''
            && model.board[cell[0] - 1][cell[1]] === '') {
            model.moves[model.moves.length] = [cell[0] - 2, cell[1]]
        }
        if (cell[1] !== 7 && model.board[cell[0]-1][cell[1]+1].slice(-1) === 'b') {
            model.moves[model.moves.length] = [cell[0] - 1, cell[1] + 1]
        }
        if (cell[1] !== 0 && model.board[cell[0]-1][cell[1]-1].slice(-1) === 'b') {
            model.moves[model.moves.length] = [cell[0] - 1, cell[1] - 1]
        }
    }
}

function moveRook(cell) {
    model.moves = [];
    let color = model.selectedFigure.slice(-1);
    moveRookOnLine(color, cell[0], cell[1], 0, 1);
    let moves = model.moves;
    moveRookOnLine(color, cell[0], cell[1], 1, 0);
    for (let i = 0; i < moves.length; i++) {
        model.moves[model.moves.length] = moves[i]
    }

}

function moveRookOnLine(color, pos_i, pos_j, di, dj) {
    if (di > 0) {
        i = 0;
        j = pos_j
    }
    else{
        i = pos_i;
        j =  0
    }
        model.moves = [];
        while (i < 8 && j < 8){
            if (pos_i > i || pos_j > j){
                if (color === model.board[i][j].slice(-1)){
                    model.moves = []
                }
                else if (color !== model.board[i][j].slice(-1) && model.board[i][j] !== ''){
                    model.moves = [[i, j]]
                }
                else{
                    model.moves[model.moves.length] = [i, j];
                }
            }
            if (pos_i < i || pos_j < j){
                if (color === model.board[i][j].slice(-1)){
                    break
                }
                else if (color !== model.board[i][j].slice(-1) && model.board[i][j] !== ''){
                    model.moves[model.moves.length] = [i, j];
                    break
                }
                else{
                    model.moves[model.moves.length] = [i, j];
                }
            }
            i += di;
            j += dj
    }

}

function moveKnight(cell) {
    let i = cell[0];
    let j = cell[1];
    let figure = model.selectedFigure.slice(-1);
    let indexes = [];
    model.moves = [[i - 2 , j - 1], [i - 1, j - 2], [i - 2, j + 1], [i - 1, j + 2],
    [i + 2, j + 1], [i + 1, j + 2], [i + 2, j - 1], [i + 1, j - 2]];
    for (let k = 0; k < 8; k++){
        let lst = model.moves[k];
        if (lst[0] < 0 ||lst[0] > 7 || lst[1] < 0 || lst[1] > 7) indexes[indexes.length] = k;
        else if (figure === 'b' && model.board[lst[0]][lst[1]].slice(-1) === 'b') indexes[indexes.length] = k;
        else if (figure === 'w' && model.board[lst[0]][lst[1]].slice(-1) === 'w') indexes[indexes.length] = k;
    }
    for (let l = 0; l < indexes.length; l++) model.moves.splice(indexes[indexes.length - l - 1],1);
}

function moveBishop(cell) {
    model.moves = [];
    let color = model.selectedFigure.slice(-1);
    moveBishopOnLine(color, cell[0], cell[1], true);
    let moves = model.moves;
    moveBishopOnLine(color, cell[0], cell[1], false);
    for (let i = 0; i < moves.length; i++) {
        model.moves[model.moves.length] = moves[i]
    }
}

function moveBishopOnLine(color, pos_i, pos_j, sing) {
    let i = 0;
        model.moves = [];
        while (i < 8){
            if (sing){
                j = pos_j + (pos_i - i)
            }
            else {
                j = pos_j - (pos_i - i)
            }
            if (j >= 0 && j < 8){
                if (i < pos_i){
                    if (color === model.board[i][j].slice(-1)){
                        model.moves = []
                    }
                    else if (color !== model.board[i][j].slice(-1) && model.board[i][j] !== ''){
                        model.moves = [[i, j]]
                    }
                    else {
                        model.moves[model.moves.length] = [i, j]
                    }
                    }
                else if (i > pos_i){
                    if (color === model.board[i][j].slice(-1)) {
                        break
                    }
                    else if (color !== model.board[i][j].slice(-1) && model.board[i][j] !== ''){
                        model.moves[model.moves.length] = [i, j];
                        break
                    }
                    else{
                        model.moves[model.moves.length] = [i, j]
                    }
                }
            }
            i++;
        }
}

function moveKing(cell) {
    let i = cell[0];
    let j = cell[1];
    let figure = model.selectedFigure.slice(-1);
    let indexes = [];
    model.moves = [[i - 1, j - 1], [i - 1, j], [i + 1, j - 1], [i + 1, j],
    [i + 1, j + 1], [i, j - 1], [i - 1, j + 1], [i, j + 1]];
    for (let k = 0; k < 8; k++){
        let moves = model.moves[k];
        if (moves[0] < 0 || moves[0] > 7  || moves[1] < 0 || moves[1] > 7) indexes[indexes.length] = k;
        else if (figure === 'b' && model.board[moves[0]][moves[1]].slice(-1) === 'b') indexes[indexes.length] = k;
        else if (figure === 'w' && model.board[moves[0]][moves[1]].slice(-1) === 'w') indexes[indexes.length] = k;
    }
    for (let l = 0; l < indexes.length; l++) model.moves.splice(indexes[indexes.length - l - 1] , 1)
}

function moveQueen(cell) {
    moveBishop(cell);
    let moves = model.moves;
    moveRook(cell);
    for (let i = 0; i < moves.length; i++) {
        model.moves[model.moves.length] = moves[i]
    }
}

function resetModel() {
    model = {"board":[],
    "history":[],
    "selectedCells":[],
    "selectedFigure":'',
    "raund":'w',
    "moves":[],
    "choice":[71, 72, 73, 74, 75, 76, 77, 78,
    81, 82, 83, 84, 85, 86, 87, 88],
    "request":[],
    "whitePositions":[],
    "blackPositions":[]
    }
}