import os
import json
from flask import Flask
from flask import request
from flask import jsonify
from chess import *

app = Flask(__name__, static_url_path='')
root_dir = os.path.dirname(os.getcwd())


@app.route('/')
def root():
    return app.send_static_file('chess.html')


chess = Chess()
ai = AI('black', chess)


@app.route("/board")
def get_board():
    chess.reset()
    return json.dumps(chess.board)


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.route('/moves', methods=['POST'])
def make_move():
    move = request.get_json(force=True)
    try:
        chess.update(move)
        ai.update()
        return json.dumps({'board': chess.board, 'history': chess.history,
                           'black_positions': chess.get_positions(chess.board)['black'],
                           'white_positions': chess.get_positions(chess.board)['white']})
    except Exception:
        raise InvalidUsage('Bad request', status_code=400)


if __name__ == '__main__':
    app.run(debug=True)
