from chess import *
import unittest


class Test(unittest.TestCase):
    def test_pawn(self):
        test = Chess()
        board = test.board
        self.assertEqual(test.found_moves([1, 0], board), [[2, 0], [3, 0]])
        self.assertEqual(test.found_moves([6, 3], board), [[5, 3], [4, 3]])
        board[2][5] = 'pawn_b'
        board[4][5] = 'pawn_w'
        self.assertEqual(test.found_moves([2, 5], board), [[3, 5]])
        self.assertEqual(test.found_moves([4, 5], board), [[3, 5]])
        board[3][4] = 'pawn_b'
        self.assertEqual(test.found_moves([3, 4], board), [[4, 4], [4, 5]])
        self.assertEqual(test.found_moves([4, 5], board), [[3, 5], [3, 4]])

    def test_rook(self):
        test = Chess()
        board = test.board
        self.assertEqual(test.found_moves([0, 0], board), test.found_moves([7, 7], board))
        board[4][4] = 'rook_w'
        board[3][3] = 'rook_b'
        self.assertEqual(test.found_moves([3, 3], board),
                         [[3, 0], [3, 1], [3, 2], [3, 4], [3, 5], [3, 6], [3, 7], [2, 3], [4, 3] , [5, 3], [6, 3]])
        self.assertEqual(test.found_moves([4, 4], board),
                         [[4, 0], [4, 1], [4, 2], [4, 3], [4, 5], [4, 6], [4, 7], [1, 4], [2, 4], [3, 4], [5, 4]])

    def test_knight(self):
        test = Chess()
        board = test.board
        self.assertEqual(test.found_moves([0, 1], board), [[2, 2], [2, 0]])
        self.assertEqual(test.found_moves([7, 1], board), [[5, 0], [5, 2]])
        board[4][4] = 'knight_w'
        board[3][3] = 'knight_b'
        self.assertEqual(test.found_moves([4, 4], board), [[2, 3], [3, 2], [2, 5], [3, 6], [5, 6], [5, 2]])
        self.assertEqual(test.found_moves([3, 3], board), [[2, 1], [2, 5], [5, 4], [4, 5], [5, 2], [4, 1]])

    def test_bishop(self):
        test = Chess()
        board = test.board
        self.assertEqual(test.found_moves([0, 2], board), test.found_moves([7, 2], board))
        board[4][4] = 'bishop_w'
        board[3][3] = 'bishop_b'
        self.assertEqual(test.found_moves([4, 4], board),
                         [[3, 3], [5, 5], [1, 7], [2, 6], [3, 5], [5, 3]])
        self.assertEqual(test.found_moves([3, 3], board),
                         [[2, 2], [4, 4], [2, 4],  [4, 2], [5, 1], [6, 0]])

    def test_king(self):
        test = Chess()
        board = test.board
        self.assertEqual(test.found_moves([0, 3], board), test.found_moves([7, 3], board))
        board[4][4] = 'king_w'
        board[3][3] = 'king_b'
        self.assertEqual(test.found_moves([4, 4], board),
                         [[3, 3], [3, 4], [5, 3], [5, 4], [5, 5], [4, 3], [3, 5], [4, 5]])
        self.assertEqual(test.found_moves([3, 3], board),
                         [[2, 2], [2, 3], [4, 2], [4, 3], [4, 4], [3, 2], [2, 4], [3, 4]])

    def test_queen(self):
        test = Chess()
        board = test.board
        self.assertEqual(test.found_moves([0, 4], board), test.found_moves([7, 4], board))
        board[4][4] = 'queen_w'
        board[3][3] = 'queen_b'
        self.assertEqual(test.found_moves([4, 4], board),
                         [[4, 0], [4, 1], [4, 2], [4, 3], [4, 5], [4, 6], [4, 7], [1, 4], [2, 4], [3, 4], [5, 4],
                         [3, 3], [5, 5], [1, 7], [2, 6], [3, 5], [5, 3]])
        self.assertEqual(test.found_moves([3, 3], board),
                         [[3, 0], [3, 1], [3, 2], [3, 4], [3, 5], [3, 6], [3, 7], [2, 3], [4, 3] , [5, 3], [6, 3],
                          [2, 2], [4, 4], [2, 4], [4, 2], [5, 1], [6, 0]])

    def test_is_king_under_attack(self):
        test = Chess()
        board = test.board
        board[5][3] = 'rook_w'
        board[1][3] = 'bishop_b'
        self.assertEqual(test.is_king_under_attack([[1, 2], [2, 2]], board), None)
        self.assertEqual(test.is_king_under_attack([[1, 3], [2, 4]], board), True)

    def test_detection_check(self):
        test = Chess()
        test.board[5][2] = 'rook_w'
        test.board[1][3] = ''
        test.update([[5, 2], [5, 0]])
        self.assertNotEqual(test.history[len(test.history) - 1], 'Шах черным')
        test.update([[5, 0], [5, 3]])
        self.assertEqual(test.history[len(test.history) - 1], 'Шах черным')

    def test_detection_mate(self):
        test = Chess()
        test.board[5][2] = 'rook_w'
        test.board[1][3] = ''
        test.board[0][2] = 'pawn_b'
        test.board[0][4] = 'pawn_b'
        test.board[0][1] = ''
        test.update([[5, 2], [5, 0]])
        self.assertNotEqual(test.history[len(test.history) - 1], 'Мат черным')
        test.update([[5, 0], [5, 3]])
        self.assertEqual(test.history[len(test.history) - 1], 'Мат черным')

    def test_get_letter(self):
        test = Chess()
        self.assertEqual(test.get_letter(1), 'B')

    def test_reset(self):
        test = Chess()
        board = copy.deepcopy(test.board)
        history = copy.copy(test.history)
        test.update([[6, 3], [4, 3]])
        test.update([[1, 3], [3, 3]])
        self.assertNotEqual(board, test.board)
        self.assertNotEqual(history, test.history)
        test.reset()
        self.assertEqual(board, test.board)
        self.assertEqual(history, test.history)

    def test_minimax(self):
        test = Chess()
        ai = AI('black', test)
        test.board = [
            ['', '', '', '', '', '', '', ''],
            ['', '', '', '', '', '', '', ''],
            ['', '', '', '', '', '', '', ''],
            ['rook_w', '', '', '', '', '', '', ''],
            ['', '', 'knight_w', '', 'bishop_w', '', '', ''],
            ['', '', '', 'bishop_b', '', '', '', ''],
            ['', '', '', '', '', '', '', ''],
            ['', '', '', '', '', '', '', '']
        ]
        test.update([[3, 0], [3, 2]])
        ai.update()
        self.assertEqual(test.history[-1:], ['D3 - E4'])

    def test_figure_valeu(self):
        test = Chess()
        ai = AI('black', test)
        self.assertEqual(ai.figure_value('pawn_w'), 10)
        self.assertEqual(ai.figure_value('bishop_b'), ai.figure_value('knight_b'))
        self.assertEqual(ai.figure_value('rook_b'), -ai.figure_value('rook_w'))
        self.assertEqual(ai.figure_value('queen_b'), -900)
        self.assertEqual(ai.figure_value('king_w'), 90)

    def test_move_value(self):
        test = Chess()
        ai = AI('black', test)
        self.assertEqual(ai.move_value([[1, 1], [2, 1]], test.board), 0)
        board = [
            ['', '', '', '', '', '', '', ''],
            ['', '', '', '', '', '', '', ''],
            ['', '', '', '', '', '', '', ''],
            ['', '', '', '', '', '', '', ''],
            ['', '', 'knight_w', '', 'bishop_w', '', '', ''],
            ['', '', '', 'bishop_b', '', '', '', ''],
            ['', '', 'rook_w', '', '', '', '', ''],
            ['', '', '', '', '', '', '', '']
        ]
        self.assertEqual(ai.move_value([[5, 4], [4, 2]], copy.deepcopy(board)), 50)
        self.assertEqual(ai.move_value([[5, 4], [6, 2]], copy.deepcopy(board)), 30)



if __name__ == '__main__':
    unittest.main()