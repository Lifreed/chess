import copy


class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code

    def to_dict(self):
        rv = dict()
        rv['message'] = self.message
        return rv


class Chess:
    def __init__(self):
        self.board = [
            ['rook_b', 'knight_b', 'bishop_b', 'king_b', 'queen_b', 'bishop_b', 'knight_b', 'rook_b'],
            ['pawn_b', 'pawn_b', 'pawn_b', 'pawn_b', 'pawn_b', 'pawn_b', 'pawn_b', 'pawn_b'],
            ['', '', '', '', '', '', '', ''],
            ['', '', '', '', '', '', '', ''],
            ['', '', '', '', '', '', '', ''],
            ['', '', '', '', '', '', '', ''],
            ['pawn_w', 'pawn_w', 'pawn_w', 'pawn_w', 'pawn_w', 'pawn_w', 'pawn_w', 'pawn_w'],
            ['rook_w', 'knight_w', 'bishop_w', 'king_w', 'queen_w', 'bishop_w', 'knight_w', 'rook_w']
        ]
        self.history = []

    def update(self, request):
        from_, to = request
        board = copy.deepcopy(self.board)
        if to not in self.found_moves(from_, board) or self.is_king_under_attack(request, board):
            raise Exception('Нечестный ход')
        self.get_board(request)

    def found_moves(self, from_, board):
        figure = board[from_[0]][from_[1]]
        moves = []
        if figure[0:4] == 'pawn':
            moves = self.move_pawn(from_, board)
        if figure[0:6] == 'knight':
            moves = self.move_knight(from_, board)
        if figure[0:4] == 'king':
            moves =self.move_king(from_, board)
        if figure[0:5] == 'queen':
            moves = self.move_queen(from_, board)
        if figure[0:4] == 'rook':
            moves = self.move_rook(from_, board)
        if figure[0:6] == 'bishop':
            moves = self.move_bishop(from_, board)
        return moves

    def get_board(self, request):
        from_, to = request
        self.board[to[0]][to[1]] = self.board[from_[0]][from_[1]]
        self.board[from_[0]][from_[1]] = ''
        board = copy.deepcopy(self.board)
        self.move_history(request)
        self.detection_check(to, board)

    def get_positions(self, board):
        black_positions = []
        white_positions = []
        for i in range(0, 8):
            for j in range(0, 8):
                if board[i][j][-1:] == 'b':
                    black_positions.append([i, j])
                if board[i][j][-1:] == 'w':
                    white_positions.append([i, j])
        return {'black':black_positions, 'white':white_positions}

    def moves(self, color, board):
        positions = self.get_positions(board)[color]
        possible_moves = []
        for pos in positions:
            moves = self.found_moves(pos, board)
            for move in moves:
                if not self.is_king_under_attack([pos, move], board):
                    possible_moves.append([pos, move])
        return possible_moves

    def is_king_under_attack(self, request, board):
        from_, to = request
        board = copy.deepcopy(board)
        figure = board[from_[0]][from_[1]]
        color_of_figure = figure[-1:]
        board[from_[0]][from_[1]] = ''
        board[to[0]][to[1]] = figure
        if figure[-1:] == 'b':
            positions = self.get_positions(board)['white']
        else:
            positions = self.get_positions(board)['black']
        for pos in positions:
            moves = self.found_moves(pos, board)
            for step in moves:
                if board[step[0]][step[1]] == 'king_' + color_of_figure:
                    return True


    def detection_check(self, to, board):
        moves = self.found_moves(to, board)
        for move in moves:
            if board[move[0]][move[1]] == 'king_w':
                if self.detection_mate('w', board):
                    self.history.append('Мат белым')
                else:
                    self.history.append('Шах белым')
            elif board[move[0]][move[1]] == 'king_b':
                if self.detection_mate('b', board):
                    self.history.append('Мат черным')
                else:
                    self.history.append('Шах черным')

    def detection_mate(self, color, board):
        if color == 'w':
            positions = self.get_positions(board)['white']
        else:
            positions = self.get_positions(board)['black']
        for pos in positions:
            moves = self.found_moves(pos, board)
            for step in moves:
                if not self.is_king_under_attack([pos, step], board):
                    return False
        return True

    def move_history(self, request):
        from_, to = request
        self.history.append(self.get_letter(from_[1]) + str(8 - from_[0]) + ' - ' +
                            self.get_letter(to[1]) + str(8 - to[0]))

    def get_letter(self, n):
        letter = {'0': 'A', '1': 'B', '2': 'C', '3': 'D',
                  '4': 'E', '5': 'F', '6': 'G', '7': 'H'}
        return letter[str(n)]

    def move_pawn(self, from_, board):
        moves = []
        selected_figure = board[from_[0]][from_[1]]
        if selected_figure == 'pawn_b':
            if board[from_[0] + 1][from_[1]] == '':
                moves.append([from_[0] + 1, from_[1]])
            if from_[0] + 1 == 2 and board[from_[0] + 2][from_[1]] == '' and board[from_[0] + 1][from_[1]] == '':
                moves.append([from_[0] + 2, from_[1]])
            if from_[1] != 7 and board[from_[0] + 1][from_[1] + 1][-1:] == 'w':
                moves.append([from_[0] + 1, from_[1] + 1])
            if from_[1] != 0 and board[from_[0] + 1][from_[1] - 1][-1:] == 'w':
                moves.append([from_[0] + 1, from_[1] - 1])
        else:
            if board[from_[0] - 1][from_[1]] == '':
                moves.append([from_[0] - 1, from_[1]])
            if from_[0] + 1 == 7 and board[from_[0] - 2][from_[1]] == '' and board[from_[0] - 1][from_[1]] == '':
                moves.append([from_[0] - 2, from_[1]])
            if from_[1] != 7 and board[from_[0] - 1][from_[1] + 1][-1:] == 'b':
                moves.append([from_[0] - 1, from_[1] + 1])
            if from_[1] != 0 and board[from_[0] - 1][from_[1] - 1][-1:] == 'b':
                moves.append([from_[0] - 1, from_[1] - 1])
        return moves

    def move_rook(self, from_, board):
        color = board[from_[0]][from_[1]][-1:]
        moves = self.move_rook_on_line(color, from_[0], from_[1], 0, 1,  board)
        moves += self.move_rook_on_line(color, from_[0], from_[1], 1, 0, board)
        return moves

    def move_knight(self, from_, board):
        i = from_[0]
        j = from_[1]
        figure = board[i][j][-1:]
        indexes = []
        moves = [
            [i - 2, j - 1], [i - 1, j - 2], [i - 2, j + 1], [i - 1, j + 2],
            [i + 2, j + 1], [i + 1, j + 2], [i + 2, j - 1], [i + 1, j - 2]
        ]
        for move in moves:
            if move[0] < 0 or move[1] < 0 or move[0] > 7 or move[1] > 7: indexes.append(move)
            elif figure == board[move[0]][move[1]][-1:]: indexes.append(move)
        for i in indexes: moves.remove(i)
        return moves

    def move_bishop(self, from_, board):
        color = board[from_[0]][from_[1]][-1:]
        moves = self.move_bishop_on_line(color, from_[0], from_[1], False, board)
        moves += self.move_bishop_on_line(color, from_[0], from_[1], True, board)
        return moves

    def move_king(self, from_, board):
        i = from_[0]
        j = from_[1]
        figure = board[from_[0]][from_[1]][-1:]
        indexes = []
        moves = [
            [i - 1, j - 1], [i - 1,+ j], [i + 1, + j - 1], [i + 1, j],
            [i + 1, j + 1], [i, j - 1], [i - 1, j + 1], [i, j + 1]
        ]
        for move in moves:
            if move[0] < 0 or move[1] < 0 or move[0] > 7 or move[1] > 7:
                indexes.append(move)
            elif figure == board[move[0]][move[1]][-1:]:
                indexes.append(move)
        for i in indexes: moves.remove(i)
        return moves

    def move_queen(self, from_, board):
        moves = self.move_rook(from_, board)
        moves += self.move_bishop(from_, board)
        return moves

    def move_rook_on_line(self, color, pos_i, pos_j, di, dj, board):
        if di > 0:
            i, j = 0, pos_j
        else:
            i, j = pos_i, 0
        moves = []
        while i < 8 and j < 8:
            if pos_i > i or pos_j > j:
                if color == board[i][j][-1:]:
                    moves = []
                elif color != board[i][j][-1:] and board[i][j] != '':
                    moves = [[i, j]]
                else:
                    moves.append([i, j])
            if pos_i < i or pos_j < j:
                if color == board[i][j][-1:]:
                    return moves
                elif color != board[i][j][-1:] and board[i][j] != '':
                    moves.append([i, j])
                    return moves
                moves.append([i, j])
            i += di
            j += dj
        return moves

    def move_bishop_on_line(self, color, pos_i, pos_j, sing, board):
        i = 0
        moves = []
        while i < 8:
            if sing:
                j = pos_j + (pos_i - i)
            else:
                j = pos_j - (pos_i - i)
            if j in range(0, 8):
                if i < pos_i:
                    if color == board[i][j][-1:]:
                        moves = []
                    elif color != board[i][j][-1:] and board[i][j] != '':
                        moves = [[i, j]]
                    else:
                        moves.append([i, j])
                elif i > pos_i:
                    if color == board[i][j][-1:]:
                        return moves
                    elif color != board[i][j][-1:] and board[i][j] != '':
                        moves.append([i, j])
                        return moves
                    else:
                        moves.append([i, j])
            i += 1
        return moves

    def reset(self):
        self.board = [
            ['rook_b', 'knight_b', 'bishop_b', 'king_b', 'queen_b', 'bishop_b', 'knight_b', 'rook_b'],
            ['pawn_b', 'pawn_b', 'pawn_b', 'pawn_b', 'pawn_b', 'pawn_b', 'pawn_b', 'pawn_b'],
            ['', '', '', '', '', '', '', ''],
            ['', '', '', '', '', '', '', ''],
            ['', '', '', '', '', '', '', ''],
            ['', '', '', '', '', '', '', ''],
            ['pawn_w', 'pawn_w', 'pawn_w', 'pawn_w', 'pawn_w', 'pawn_w', 'pawn_w', 'pawn_w'],
            ['rook_w', 'knight_w', 'bishop_w', 'king_w', 'queen_w', 'bishop_w', 'knight_w', 'rook_w']
        ]
        self.history = []


class AI:
    def __init__(self, color, chess):
        self.color = color
        self.chess = chess

    def update(self):
        if self.chess.history[len(self.chess.history) - 1] != 'Мат черным':
            board = copy.deepcopy(self.chess.board)
            request = self.position_evaluation(board)
            self.chess.update(request)

    def position_evaluation(self, board):
        possible_moves = self.chess.moves(self.color, board)
        best_move = []
        best_value = 9999
        for move in possible_moves:
            move_value = self.minimax(1, move, True, copy.deepcopy(board))
            if move_value < best_value:
                best_value = move_value
                best_move = move
        return best_move

    def move_value(self, move, board):
        from_, to = move
        board[to[0]][to[1]] = board[from_[0]][from_[1]]
        board[from_[0]][from_[1]] = ''
        move_value = 0
        for i in range(0, 8):
            for j in range(0, 8):
                move_value += self.figure_value(board[i][j])
        return move_value

    def minimax(self, depth, request, player, board):
        from_, to = request
        if depth == 0:
            return self.move_value(request, board)
        best_value = self.move_value(request, copy.deepcopy(board))
        board[to[0]][to[1]] = board[from_[0]][from_[1]]
        board[from_[0]][from_[1]] = ''
        if player:
            moves = self.chess.moves('white', board)
            for move in moves:
                best_value = max(best_value, self.minimax(depth - 1, move, not player, copy.deepcopy(board)))
            return best_value
        else:
            moves = self.chess.moves('black', board)
            for move in moves:
                best_value = min(best_value, self.minimax(depth - 1, move, not player, copy.deepcopy(board)))
            return best_value

    def figure_value(self, figure):
        value = {
            'pawn': 10, 'rook': 50, 'knight': 30,
            'bishop': 30, 'king': 90, 'queen': 900,
            '': 0, 'b': -1, 'w': 1
        }
        return value[figure[0:-2]] * value[figure[-1:]]

